import Animation3 from "../public/animations/personalLoan.json";
import BuisnessLoan from "../public/animations/BuisnessLoan.json";
import BuyNowAnimation from "../public/animations/BuyNowPayLater.json";
import MicroLoans from "../public/animations/MicroLoans.json";
import HomeLoans from "../public/animations/HomeLoan.json";
import GoldLoan from "../public/animations/GoldLoan.json";
import CarLoan from "../public/animations/CarLoan.json";
import CreditImprovement from "../public/animations/CreditImprovementService.json";
import Credit from "../public/animations/credit-card.json";
import InsuranceService from "../public/animations/InsuranceServices.json";
import StockInvestment from "../public/animations/Stocks&Investment.json";
import LoanProperty from "../public/animations/LoanAgainstProperty.json";

export const Cards = [
  {
    id: 0,
    name: "Buy Now Pay Later",
    description:
      "From 35+ options , choose a card matching your lifestyle & needs",
    tag: "get Best Offers",
    animationData: BuyNowAnimation,
  },
  {
    id: 1,
    name: "Personal Loan",
    description:
      "Select the best offer currated just for you form a wide choice of Banks & NBFCs",
    tag: "Check Elligibility",
    animationData: Animation3,
  },
  {
    id: 2,
    name: "Micro Loans (under $50k)",
    description: "Instant Small ticket loans to meet your immediate cash needs",
    tag: "Get Instant Loan",
    animationData: MicroLoans,
  },
  {
    id: 3,
    name: "Buisness Loan",
    description: "Expand your buisness with loans at low interest rates",
    tag: "Check Elligibility",
    animationData: BuisnessLoan,
  },
  {
    id: 4,
    name: "Home Loan",
    description: "Expand your buisness with loans at low interest rates",
    tag: "Check Elligibility",
    animationData: HomeLoans,
  },
  {
    id: 5,
    name: "Gold Loan",
    description: "Expand your buisness with loans at low interest rates",
    tag: "Check Elligibility",
    animationData: GoldLoan,
  },
  {
    id: 6,
    name: "Car Loan",
    description: "Expand your buisness with loans at low interest rates",
    tag: "Check Elligibility",
    animationData: CarLoan,
  },
  {
    id: 7,
    name: "Credit Improvement Service",
    description: "Instant Small ticket loans to meet your immediate cash needs",
    tag: "Get Instant Loan",
    animationData: CreditImprovement,
  },
  {
    id: 8,
    name: "Credit Loan",
    description: "Instant Small ticket loans to meet your immediate cash needs",
    tag: "Get Instant Loan",
    animationData: Credit,
  },
  {
    id: 9,
    name: "Insaurence Services",
    description: "Instant Small ticket loans to meet your immediate cash needs",
    tag: "Get Instant Loan",
    animationData: InsuranceService,
  },
  {
    id: 10,
    name: "Stocks & Investment",
    description: "Instant Small ticket loans to meet your immediate cash needs",
    tag: "Get Instant Loan",
    animationData: StockInvestment,
  },
  {
    id: 11,
    name: "Loans against Property",
    description: "Instant Small ticket loans to meet your immediate cash needs",
    tag: "Get Instant Loan",
    animationData: LoanProperty,
  },
];
